import { Redis } from 'ioredis';
import logger from '../../utils/logger';
import APP_CONFIG from '../app.config';

export default class RedisConfig {
  static client: Redis;
  static instance: RedisConfig;

  constructor() {
    if (RedisConfig.instance) {
      return RedisConfig.instance;
    }
    RedisConfig.client = new Redis({
      host: APP_CONFIG.ENV.DATABASE.REDIS.HOST,
      port: APP_CONFIG.ENV.DATABASE.REDIS.PORT,
      password: APP_CONFIG.ENV.DATABASE.REDIS.PASSWORD,
      db: APP_CONFIG.ENV.DATABASE.REDIS.DATABASE,
    });
    RedisConfig.client.on('error', (err) => {
      logger.error(err.stack);
      logger.error(`[Database][Redis] Database connection error.`);
      process.exit();
    });

    RedisConfig.client.on('connect', () => {
      logger.info(`[Database][Redis] Database has connected successfully!`);
    });

    RedisConfig.instance = this;
  }
}
