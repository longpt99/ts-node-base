import { Redis, RedisKey } from 'ioredis';

export class CacheManagerUtil {
  private static instance: CacheManagerUtil;
  public client: Redis;

  constructor(client: Redis) {
    if (CacheManagerUtil.instance) {
      return CacheManagerUtil.instance;
    }

    this.client = client;
    CacheManagerUtil.instance = this;
  }

  async setKey(params: { key: string; value: string; exp?: number }) {
    return this.client.setex(params.key, params.exp ?? -1, params.value);
  }

  async delKey(...key: RedisKey[]) {
    return this.client.del(key);
  }

  async getKey(key: string) {
    return this.client.get(key);
  }

  async getKeys(key: string) {
    return this.client.keys(key);
  }

  async hashSet(key: string, field: string, value: string) {
    return this.client.hset(key, field, value);
  }

  async hashGet(key: string, field: string) {
    return this.client.hget(key, field);
  }

  async hashGetAll(key: string) {
    return this.client.hgetall(key);
  }

  async hashDel(key: string, field: string) {
    return this.client.hdel(key, field);
  }

  // async push<T>(queue: string, dataArr: T[]): Promise<number> {
  //   const msgArr: string[] = [];
  //   for (let i = 0; i < dataArr.length; i++) {
  //     msgArr.push(JSON.stringify(dataArr[i]));
  //   }
  //   return new Promise((resolve, reject) => {
  //     this.client.rpush(queue, msgArr, (err, val) => {
  //       if (err) {
  //         reject(err);
  //       }
  //       resolve(val);
  //     });
  //   });
  // }
}
